﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Rigidbody2D rb;
    public int health = 20;
    public float jumpDelay=1.2f;
    public float deflectDelay=1.2f;
    public KeyCode jumpKey;
    public KeyCode deflectKey;
    public float jumpHeight = 1;
    private bool canJump = false;
    private EnemyCatchingTrigger ect;
    float initialWidth=0.3f;
    float initialArea = 0.7f;
    internal float detectionWidth = 0.3f;
    internal float detectionArea = 0.7f;
    private BallSpawner ballSpawner;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        ect = FindObjectOfType<EnemyCatchingTrigger>();
        ballSpawner = FindObjectOfType<BallSpawner>();
    }

    void Update () {  
        if (Input.GetKeyDown(jumpKey) &&   canJump)
        {
            rb.velocity = Vector2.up * jumpHeight;
            canJump = false;
            // Add sound
            // Change Animation
        }
        if (Input.GetKeyDown(deflectKey))
        {
            DetectingBall();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        canJump = true;
    }

    void DetectingBall()
    {
        Vector2 startingPoint = (Vector2)transform.position + new Vector2(0.5f, 0.2f);
        Collider2D col = Physics2D.OverlapBox(startingPoint, new Vector2(detectionWidth, 1), 0);
        if (col != null)
        {
            if (col.GetComponent<Ball>())
            {
                Ball b = col.GetComponent<Ball>();
                if (b.isCounterable)
                {
                    b.isPlayerSide = true;
                    // for reflecting
                    if (Vector2.Distance(transform.position, b.transform.position) < detectionArea)
                    {
                        b.rb.velocity = new Vector2(10, 0);
                        CheckBallType(b,true);
                    }
                    // for deflecting
                    else
                    {
                        b.rb.velocity = new Vector2(10, 6);
                        CheckBallType(b);
                    }
                }
                detectionArea = initialArea;
                detectionWidth = initialWidth;
                ballSpawner.DespawnBall();
            }
        }
    }



    public void GetHit(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            gameObject.SetActive(false);
            ballSpawner.StopBallSpawning();
        }
        ballSpawner.DespawnBall();
    }

    public void CheckBallType(Ball ball,bool critical = false)
    {
        switch (ball.ballName) {
            case "GreenBall":
                if (critical)
                {
                    health += 1;
                }
                else
                {
                    health += Mathf.FloorToInt(health * 0.15f);
                }
                break;
            case "BigGreenBall":
                if (critical)
                {
                    health += 2;
                }
                else
                {
                    health += Mathf.FloorToInt(health * 0.3f);
                }
                break;
            case "BlueBall":
                ect.blueBallEffect = true;
                detectionWidth = initialWidth * 1.15f;
                detectionArea = initialArea*1.15f;
                break;
            case "BigBlueBall":
                ect.blueBallEffect = true;
                detectionWidth = initialWidth * 1.15f;
                detectionArea = initialArea * 1.15f;
                break;
        }

    }

}
