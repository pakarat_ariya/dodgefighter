﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Ball",menuName ="Ball/Create a new ball",order =1)]
public class BallScriptableObjects : ScriptableObject {
    public string ballName = "New ball";
    public Sprite ballSprite;
    public bool isCouterable = true;
    public int damage = 5;
    public float speed = 30f;
    public float chance = 1f;
}
