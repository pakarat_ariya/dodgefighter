﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDespawner : MonoBehaviour {

    BallSpawner ballSpawner;

    private void Awake()
    {
        ballSpawner = FindObjectOfType<BallSpawner>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Ball>())
        {
            if (ballSpawner)
            {
                ballSpawner.DespawnBall();
            }
            else
            {
                print("NONO");
            }
        }
    }

}
