﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour {
    public BallScriptableObjects ballObj;
    
    public bool isPlayerSide = false;
    public bool isHit = false;
    public float chance;
    internal Rigidbody2D rb;
    internal int damage;
    internal bool isCounterable;
    internal float speed;
    internal string ballName;
    private float destroyTime = 10f;
    

    private void Awake()
    {
        
        damage = ballObj.damage;
        isCounterable = ballObj.isCouterable;
        speed = ballObj.speed;
        chance = ballObj.chance;
        ballName = ballObj.ballName;
        rb = GetComponent<Rigidbody2D>();
        Invoke("DestroyBall", destroyTime);
    }

    public Ball SetBallInstant(BallScriptableObjects obj)
    {
        ballObj = obj;
        GetComponent<SpriteRenderer>().sprite = obj.ballSprite;
        damage = obj.damage;
        isCounterable = obj.isCouterable;
        speed = obj.speed;
        chance = obj.chance;
        ballName = obj.ballName;
        if (obj.ballName.Substring(0, 3) == "Big")
        {
            transform.localScale *= 2;
        }
        return this;
    }

    private void Update()
    {
        if (isHit)
        {
            DestroyBall();
        }
    }

    public void Throwball(float speedFactor=1, bool blueBall = false, string effect = "")
    {
        if (!blueBall)
        {
            rb.velocity = new Vector2(-speed * speedFactor, 0);
        }
        else
        {
            damage *= 2;

            rb.velocity = new Vector2(-speed/2 * speedFactor, 0);
        }
    }

    private void DestroyBall()
    {
        Destroy(gameObject);
    }
}
