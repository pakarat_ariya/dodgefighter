﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour {
    
    public List<Ball> balls=new List<Ball>();
    BallScriptableObjects lastBall=null;
    public float interval = 2.0f;
    internal bool spawning = true;
    float timer=2f;
    public List<BallScriptableObjects> ballObjs = new List<BallScriptableObjects>();
    public Ball ballPref;
    bool isBallInScene = false;

    private void Awake()
    {
        ballPref = Resources.Load<Ball>("Dodgeball");
        ballObjs.AddRange(Resources.LoadAll<BallScriptableObjects>("BallObjects"));
        foreach (BallScriptableObjects ball in ballObjs)
        {
            Ball newBall = ballPref;
            newBall.ballObj = ball;
            balls.Add(newBall);
            
        }
    }


    private void FixedUpdate()
    {
        timer += Time.fixedDeltaTime;
        if (timer >= interval&&spawning&&!isBallInScene)
        {
            // Random Ball
            BallScriptableObjects ball=RandomBall();
            while (ball == lastBall && ball.ballName != "RedBall")
            {
                ball = RandomBall();
            }
            lastBall = ball;
            // Spawn the ball
            Ball obj = Instantiate<Ball>(ballPref);
            obj.SetBallInstant(ball);
            obj.transform.position = transform.position;
            isBallInScene = true;
            // Set ball Speed
            timer = 0;
        }
    }

    private BallScriptableObjects RandomBall()
    {
        BallScriptableObjects returningBall = null;
        float totalChance = 0;
        foreach (BallScriptableObjects ball in ballObjs)
        {
            totalChance += ball.chance;
        }
        float randomFloat = Random.Range(0f, totalChance);
        float currentChance = 0f;
        foreach (BallScriptableObjects ball in ballObjs)
        {
            currentChance += ball.chance;
            if (randomFloat <= currentChance)
            {
                returningBall = ball;
                break;
            } 
        }
       
        return returningBall;
    }

    public void StopBallSpawning()
    {
        spawning = false;
        foreach (Ball b in FindObjectsOfType<Ball>())
        {
            Destroy(b.gameObject);
        }
    }

    public void DespawnBall()
    {
        isBallInScene = false;
    }
}
