﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCatchingTrigger : MonoBehaviour {

	private Enemy enemy;
    internal bool blueBallEffect = false;
    private Player player;

    private void Awake()
    {
        player = FindObjectOfType<Player>();
        enemy = GetComponentInParent<Enemy>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Ball>())
        {

            Ball ball = col.GetComponent<Ball>();
            if (!ball.isPlayerSide)
            {
                ball.rb.gravityScale = 0;
                ball.rb.velocity = Vector2.zero;
                // get blueball effect from player
                StartCoroutine(enemy.PrepareToThrow(ball,blueBallEffect));
                blueBallEffect = false;
            }

        }
    }
}
