﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBody : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Ball>())
        {
            Ball ball = col.GetComponent<Ball>();
            if (ball.isPlayerSide)
            {
                GetComponentInParent<Enemy>().GetHit(ball.damage);
                ball.isHit = true;
            }
        }
    }
}
