﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public int health=20;
    public float speedFactor = 0.5f;
    public float ballDelay = 0.3f;
    private BallSpawner ballSpawner;

    private void Awake()
    {
        ballSpawner = GetComponentInChildren<BallSpawner>();
    }

    public IEnumerator PrepareToThrow(Ball ball,bool blueBall =false)
    {
        yield return new WaitForSeconds(ballDelay);
        // Do Animation
        // Change Animation
        // Throw the ball
        ball.transform.position = transform.position;
        ball.Throwball(speedFactor,blueBall);

    }

    public void GetHit(int damage) {
        health -= damage;
        if (health <= 0)
        {
            ballSpawner.StopBallSpawning();
            gameObject.SetActive(false);
        }
    }

}
